<?php

/**
 * VideoPage frontend video controller
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_ListController extends Mage_Core_Controller_Front_Action
{

    /**
     * Initialize requested video category object
     *
     * @return  Jworks_VideoPage_Model_Category
     */
    protected function _initVideoList()
    {
        $categoryId = (int)$this->getRequest()->getParam('cid');
        if ($categoryId) {
            $video_category = Mage::getModel('videopage/category')
                ->load($categoryId);
            Mage::register('current_video_category', $video_category);
        } else {
            $category_collection = Mage::getModel('videopage/category')->getCollection();
            if ($category_collection->count() > 0)
                Mage::register('current_video_category', Mage::getModel('videopage/category')->getCollection()->getFirstItem());
        }
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->_initVideoList();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     *
     */
    public function popupAction()
    {
        $video_id = (int)$this->getRequest()->getParam('id');
        Mage::register('video_id', $video_id);
        $this->loadLayout(false);
        $this->renderLayout();
    }

}