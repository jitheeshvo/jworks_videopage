<?php

/**
 * VideoPage adminhtml video controller
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Adminhtml_Videopage_VideosController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {

        $this->loadLayout()
            ->_setActiveMenu('videopage/videos')
            ->_addBreadcrumb(Mage::helper('videopage')->__('Video Manager'), Mage::helper('videopage')->__('Video Manager'));

        return $this;
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('videopage/list')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('video_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('videopage/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Video Manager'), Mage::helper('videopage')->__('Video Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Video Manager'), Mage::helper('videopage')->__('Video Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('videopage/adminhtml_videos_edit'))
                ->_addLeft($this->getLayout()->createBlock('videopage/adminhtml_videos_edit_tabs'));
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Video does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     *
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     *
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            try {
                if (isset($data['video_thumbnail']['delete']) && $data['video_thumbnail']['delete'] == 1) {
                    $path = Mage::getBaseDir('media') . DS . 'videos' . DS;
                    $filname_delete = str_replace('/', '\\', $data['video_thumbnail']['value']);
                    if (file_exists($path . $filname_delete)) {

                        @unlink($path . $filname_delete);
                    }
                    $data['video_thumbnail'] = '';
                } elseif (isset($data['video_thumbnail']['value']) && $data['video_thumbnail']['value']) {
                    $data['video_thumbnail'] = $data['video_thumbnail']['value'];
                }

                if (isset($_FILES['video_thumbnail']['name']) && $_FILES['video_thumbnail']['name'] != '') {
                    try {
                        /* Starting upload */
                        $uploader = new Varien_File_Uploader('video_thumbnail');

                        // Any extention would work
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);

                        // Set the file upload mode
                        // false -> get the file directly in the specified folder
                        // true -> get the file in the product like folders
                        //	(file.jpg will go in something like /media/f/i/file.jpg)
                        $uploader->setFilesDispersion(false);

                        // We set media as the upload dir
                        $path = Mage::getBaseDir('media') . DS . 'videos' . DS;
                        $uploader->save($path, $_FILES['video_thumbnail']['name']);
                    } catch (Exception $e) {

                    }

                    //this way the name is saved in DB
                    $data['video_thumbnail'] = 'videos/' . $_FILES['video_thumbnail']['name'];
                }
                $model = Mage::getModel('videopage/list');
                $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('videopage')->__('Video was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Unable to find video to save'));
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('videopage/list');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('videopage')->__('Video was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function massDeleteAction()
    {
        $blockIds = $this->getRequest()->getParam('videos');
        if (!is_array($blockIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Please select item(s)'));
        } else {
            try {
                foreach ($blockIds as $blockId) {
                    $block = Mage::getModel('videopage/list')->load($blockId);
                    $block->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($blockIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     *
     */
    public function massStatusAction()
    {
        $blockIds = $this->getRequest()->getParam('videos');
        if (!is_array($blockIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($blockIds as $blockId) {
                    $block = Mage::getSingleton('videopage/list')
                        ->load($blockId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($blockIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

}