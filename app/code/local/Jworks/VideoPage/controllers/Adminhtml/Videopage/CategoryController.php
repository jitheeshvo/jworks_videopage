<?php

/**
 * VideoPage adminhtml video category controller
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Adminhtml_Videopage_CategoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {

        $this->loadLayout()
            ->_setActiveMenu('videopage/category')
            ->_addBreadcrumb(Mage::helper('videopage')->__('Video Manager'), Mage::helper('videopage')->__('Video Category Manager'));
        return $this;
    }

    /**
     *
     */
    protected function _initCategory()
    {
        $id = $this->getRequest()->getParam('id');
        $filter = $this->getRequest()->getParam('filter');
        $category = Mage::getModel('videopage/category');
        if ($id) {
            $category = $category->load($id);
        }
        if (isset($filter))
            $category->setCategoryReadonly(false);
        else
            $category->setCategoryReadonly(true);
        Mage::register('video_category', $category);
    }


    /**
     *
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     *
     */
    public function editAction()
    {
        $this->_initCategory();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('videopage/category')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('video_category_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('videopage/items');

            $this->_addBreadcrumb(Mage::helper('videopage')->__('Video Category Manager'), Mage::helper('videopage')->__('Video Category Manager'));
            $this->_addBreadcrumb(Mage::helper('videopage')->__('Video Category Manager'), Mage::helper('videopage')->__('Video Category Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('videopage/adminhtml_category_edit'))
                ->_addLeft($this->getLayout()->createBlock('videopage/adminhtml_category_edit_tabs'));
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Category does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     *
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     *
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $category = $this->getRequest()->getPost('category');
            $data['videos'] = Mage::helper('adminhtml/js')->decodeGridSerializedInput($category['videos']);
            $model = Mage::getModel('videopage/category');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }

                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('videopage')->__('Category was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Unable to find category to save'));
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('videopage/category');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('videopage')->__('Category was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function massDeleteAction()
    {
        $blockIds = $this->getRequest()->getParam('video_category');
        if (!is_array($blockIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('videopage')->__('Please select item(s)'));
        } else {
            try {
                foreach ($blockIds as $blockId) {
                    $block = Mage::getModel('videopage/category')->load($blockId);
                    $block->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($blockIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     *
     */
    public function massStatusAction()
    {
        $blockIds = $this->getRequest()->getParam('video_category');
        if (!is_array($blockIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($blockIds as $blockId) {
                    $block = Mage::getSingleton('videopage/category')
                        ->load($blockId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($blockIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Get videos grid and serializer block
     */
    public function videosAction()
    {
        $this->_initCategory();
        $this->loadLayout();
        $this->getLayout()->getBlock('category.edit.tab.videos')
            ->setSelectedVideos($this->getRequest()->getPost('category_videos', null));
        $this->renderLayout();
    }

    /**
     * Get videos grid
     */
    public function videoGridAction()
    {
        $this->_initCategory();
        $this->loadLayout();
        $this->getLayout()->getBlock('category.edit.tab.videos')
            ->setSelectedVideos($this->getRequest()->getPost('category_videos', null));
        $this->renderLayout();
    }

}