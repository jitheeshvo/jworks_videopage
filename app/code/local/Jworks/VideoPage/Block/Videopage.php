<?php

/**
 * Video page block
 *
 * @category   Jworks
 * @package    Jworks\VideoPage
 * @subpackage Block
 * @since      0.0.1
 */
class Jworks_VideoPage_Block_Videopage extends Jworks_VideoPage_Block_Videopage_Abstract
{
    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getVideo()
    {
        $video_id = Mage::registry('video_id');
        $video = Mage::getModel('videopage/list')->load($video_id);
        return $video;
    }

}

?>