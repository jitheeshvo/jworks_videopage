<?php

/**
 * Video listpage block
 *
 * @category   Jworks
 * @package    Jworks\VideoPage
 * @subpackage Block
 * @since      0.0.1
 */
class Jworks_VideoPage_Block_List extends Jworks_VideoPage_Block_Videopage_Abstract
{

    /**
     * @var
     */
    protected $_videosCollection;
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'videopage/list_toolbar';

    /**
     * Function to get the video categories
     *
     * @return array
     */
    public function getLoadedVideoCollection()
    {
        return $this->_getVideosCollection();
    }

    /**
     * @return mixed
     */
    protected function _getVideosCollection()
    {
        if (is_null($this->_videosCollection)) {
            $videoCollection = Mage::getModel('videopage/list')->getCollection()->addFieldToFilter('status', 1);
            if ($this->getVideoCategory()) {
                $videos_to_filter = Mage::getResourceModel('videopage/category')->getCategoryVideos($this->getVideoCategory()->getCategoryId());
                $video_to_filter_string = "'" . str_replace(",", "','", implode(',', $videos_to_filter)) . "'";;
                $videoCollection->addFieldToFilter('video_id', array('in' => $videos_to_filter));
                $videoCollection->getSelect()->reset(Zend_Db_Select::ORDER)->order(new Zend_Db_Expr("FIELD(video_id,$video_to_filter_string)"));
            }
            $this->_videosCollection = $videoCollection;
        }
        return $this->_videosCollection;
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getVideosCollection();

        // set collection to toolbar
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        $this->_getVideosCollection()->load();
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Jworks_VideoPage_Block_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

}

?>