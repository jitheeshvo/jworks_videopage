<?php

/**
 * VideoPage adminhtml video grid container
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Category constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_category';
        $this->_blockGroup = 'videopage';
        $this->_headerText = Mage::helper('videopage')->__('Video Category');
        $this->_addButtonLabel = Mage::helper('videopage')->__('Add Video Category');
        parent::__construct();
    }

}