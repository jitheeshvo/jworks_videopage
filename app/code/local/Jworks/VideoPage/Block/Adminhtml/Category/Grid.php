<?php

/**
 * VideoPage adminhtml video category grid
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */

class Jworks_VideoPage_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Category_Grid constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->setId('videoCategoryGrid');
        $this->setDefaultSort('category_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('videopage/category')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('category_id', array(
            'header' => Mage::helper('videopage')->__('ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'category_id',
        ));
        $this->addColumn('category_title', array(
            'header' => Mage::helper('videopage')->__('Category Title'),
            'align' => 'left',
            'width' => '700px',
            'index' => 'category_title',
            'type' => 'text',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('videopage')->__('Status'),
            'align' => 'left',
            'width' => '120px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('videopage')->__('Action'),
                'width' => '120',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('videopage')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));


        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('block_id');
        $this->getMassactionBlock()->setFormFieldName('video_category');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('videopage')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('videopage')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('videopage/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));

        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}