<?php

/**
 * VideoPage adminhtml category add/edit form
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Category_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Category_Edit_Tab_Form constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $videoCategoryTitle = Mage::getModel('videopage/category')->load(Mage::registry('video_category_data')->getCategoryId())->getCategoryTitle();
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('video_category_form', array('legend' => Mage::helper('videopage')->__("Video Category Title : %s", $this->htmlEscape($videoCategoryTitle))));

        $fieldset->addField('category_title', 'text', array(
            'label' => Mage::helper('videopage')->__('Category Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'category_title',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('videopage')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('videopage')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('videopage')->__('Disabled'),
                ),
            ),
        ));

        if (Mage::getSingleton('adminhtml/session')->getVideoCategoryData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getVideoCategoryData());
            Mage::getSingleton('adminhtml/session')->setVideoCategoryData(null);
        } elseif (Mage::registry('video_category_data')) {
            $form->setValues(Mage::registry('video_category_data')->getData());
        }
        return parent::_prepareForm();
    }

}