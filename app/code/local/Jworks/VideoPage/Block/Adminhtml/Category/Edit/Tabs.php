<?php

/**
 * VideoPage adminhtml video category add/edit form tabs
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Category_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('video_category_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('videopage')->__('Video Category Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('videopage')->__('Video Category Information'),
            'title' => Mage::helper('videopage')->__('Video Category Information'),
            'content' => $this->getLayout()->createBlock('videopage/adminhtml_category_edit_tab_form')->toHtml(),
        ));

        $this->addTab('video_grid_section', array(
            'label' => Mage::helper('videopage')->__('Videos'),
            'url' => $this->getUrl('*/*/videos', array('_current' => true)),
            'class' => 'ajax',
        ));

        return parent::_beforeToHtml();
    }

}