<?php

/**
 * VideoPage adminhtml video category grid form container
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Category_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'videopage';
        $this->_controller = 'adminhtml_category';

        $this->_updateButton('save', 'label', Mage::helper('videopage')->__('Save Category'));
        $this->_updateButton('delete', 'label', Mage::helper('videopage')->__('Delete Category'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('video_category_data') && Mage::registry('video_category_data')->getId()) {
            return Mage::helper('videopage')->__("Edit Category");
        } else {
            return Mage::helper('videopage')->__('Add New Category');
        }
    }

}