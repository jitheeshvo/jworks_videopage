<?php

/**
 * VideoPage adminhtml video add/edit form
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Videos_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos_Edit_Tab_Form constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $videoTitle = Mage::getModel('videopage/list')->load(Mage::registry('video_data')->getVideoId())->getVideoTitle();
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('video_form', array('legend' => Mage::helper('videopage')->__("Video Title : '%s'", $this->htmlEscape($videoTitle))));

        $fieldset->addField('video_title', 'text', array(
            'label' => Mage::helper('videopage')->__('Video Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'video_title',
        ));

        try {
            $config = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
            $config->setData(Mage::helper('videopage')->recursiveReplace(
                '/videopage/',
                '/' . (string)Mage::app()->getConfig()->getNode('admin/routers/videos_admin/args/frontName') . '/',
                $config->getData()
            )
            );
        } catch (Exception $ex) {
            $config = null;
        }

        $fieldset->addField('video_description', 'editor', array(
            'label' => Mage::helper('videopage')->__('Video Description'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'video_description',
            'style' => 'width:700px; height:100px;',
            'config' => $config,
        ));

        $fieldset->addField('video_thumbnail', 'image', array(
            'label' => Mage::helper('videopage')->__('Video Thumbnail'),
            'required' => true,
            'name' => 'video_thumbnail',
        ));

        $fieldset->addField('video_embed_code', 'textarea', array(
            'label' => Mage::helper('videopage')->__('Youtube ID / Embed Code'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'video_embed_code',
            'style' => 'width:700px; height:100px;',
            'config' => $config,
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('videopage')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('videopage')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('videopage')->__('Disabled'),
                ),
            ),
        ));

        if (Mage::getSingleton('adminhtml/session')->getVideoData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getVideoData());
            Mage::getSingleton('adminhtml/session')->setVideoData(null);
        } elseif (Mage::registry('video_data')) {
            $form->setValues(Mage::registry('video_data')->getData());
        }
        return parent::_prepareForm();
    }

}