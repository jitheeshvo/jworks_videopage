<?php

/**
 * VideoPage adminhtml video grid
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
/**
 * Class Jworks_VideoPage_Block_Adminhtml_Videos_Category_Grid
 */
class Jworks_VideoPage_Block_Adminhtml_Videos_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos_Category_Grid constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->setId('videoGrid');
        $this->setDefaultSort('video_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);

        if ($this->getVideosData()->getCategoryId()) {
            $this->setDefaultFilter(array('selected_videos' => 1));
        }
    }

    /**
     * @return mixed
     */
    public function getVideosData()
    {
        return Mage::registry('video_category');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('videopage/list')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add filter
     *
     * @param object $column
     * @return Jworks_VideoPage_Block_Adminhtml_Videos_Category_Grid
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'selected_videos') {
            $videoIds = $this->_getSelectedVideos();
            if (empty($videoIds)) {
                $videoIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('video_id', array('in' => $videoIds));
            } else {
                if ($videoIds) {
                    $this->getCollection()->addFieldToFilter('video_id', array('nin' => $videoIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('selected_videos', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'selected_videos',
            'values' => $this->_getSelectedVideos(),
            'align' => 'center',
            'index' => 'video_id'
        ));
        $this->addColumn('video_id', array(
            'header' => Mage::helper('videopage')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'video_id',
        ));
        $this->addColumn('video_title', array(
            'header' => Mage::helper('videopage')->__('Video Title'),
            'align' => 'left',
            'width' => '300px',
            'index' => 'video_title',
            'type' => 'text',
        ));
        $this->addColumn('video_description', array(
            'header' => Mage::helper('videopage')->__('Video Description'),
            'align' => 'left',
            'width' => '150px',
            'index' => 'video_description',
            'type' => 'text',
        ));
        $this->addColumn('position', array('header' => Mage::helper('videopage')->__('Position'),
            'width' => 80,
            'validate_class' => 'validate-number',
            'type' => 'number',
            'index' => 'position',
            'editable' => true
        ));

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/videoGrid', array('_current' => true));
    }

    /**
     * Retrieve selected videos
     *
     * @return array
     */
    protected function _getSelectedVideos()
    {
        $selected_videos = array_keys($this->getSelectedVideos());
        return $selected_videos;
    }

    /**
     * @return array
     */
    public function getSelectedVideos()
    {
        $selected_videos = array();
        $selected_videos = $this->getVideosData()->getVideoIds();
        return $selected_videos;
    }

}