<?php

/**
 * VideoPage adminhtml video add/edit form tabs
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Videos_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('videos_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('videopage')->__('Video Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('videopage')->__('Video Information'),
            'title' => Mage::helper('videopage')->__('Video Information'),
            'content' => $this->getLayout()->createBlock('videopage/adminhtml_videos_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}