<?php

/**
 * VideoPage adminhtml video grid
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Videos_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos_Grid constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->setId('videoGrid');
        $this->setDefaultSort('video_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('videopage/list')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('video_id', array(
            'header' => Mage::helper('videopage')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'video_id',
        ));
        $this->addColumn('video_title', array(
            'header' => Mage::helper('videopage')->__('Video Title'),
            'align' => 'left',
            'width' => '300px',
            'index' => 'video_title',
            'type' => 'text',
        ));
        $this->addColumn('video_description', array(
            'header' => Mage::helper('videopage')->__('Video Description'),
            'align' => 'left',
            'width' => '150px',
            'index' => 'video_description',
            'type' => 'text',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('videopage')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('videopage')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('videopage')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));


        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('block_id');
        $this->getMassactionBlock()->setFormFieldName('videos');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('videopage')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('videopage')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('videopage/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));

        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}