<?php

/**
 * VideoPage adminhtml video grid form container
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Videos_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'videopage';
        $this->_controller = 'adminhtml_videos';

        $this->_updateButton('save', 'label', Mage::helper('videopage')->__('Save Video'));
        $this->_updateButton('delete', 'label', Mage::helper('videopage')->__('Delete Video'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('video_description') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'video_description');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'video_description');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('video_data') && Mage::registry('video_data')->getId()) {
            return Mage::helper('videopage')->__("Edit Video Preference");
        } else {
            return Mage::helper('videopage')->__('Add New Video');
        }
    }

}