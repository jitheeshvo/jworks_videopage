<?php

/**
 * VideoPage adminhtml video grid container
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Block_Adminhtml_Videos extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Jworks_VideoPage_Block_Adminhtml_Videos constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_videos';
        $this->_blockGroup = 'videopage';
        $this->_headerText = Mage::helper('videopage')->__('Video Manager');
        $this->_addButtonLabel = Mage::helper('videopage')->__('Add Videos');
        parent::__construct();
    }

}