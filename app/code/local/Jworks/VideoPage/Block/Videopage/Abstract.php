<?php

/**
 * @category   Jworks
 * @package    Jworks\VideoPage
 * @subpackage Block
 * @since      0.0.1
 */
abstract class Jworks_VideoPage_Block_Videopage_Abstract extends Mage_Core_Block_Template
{
    /**
     * Function to get the config settings
     *
     * @staticvar array $pagetitle
     * @return string The pagetitle
     */
    public function getSettings($key)
    {
        static $pagetitle = null;
        if (!isset($pagetitle)) {
            $pagetitle = Mage::helper('videopage')->getConfig('settings');
        }
        return isset($pagetitle[$key]) ? $pagetitle[$key] : '';
    }

    /**
     * Function to get the video categories
     *
     * @return array
     */
    public function getVideoCategories()
    {
        $categories = Mage::getModel('videopage/category')->getCollection()->addFieldToFilter('status', 1);
        return $categories;
    }

    /**
     * @return mixed
     */
    protected function getVideoCategory()
    {
        return Mage::registry('current_video_category');
    }
}

?>
