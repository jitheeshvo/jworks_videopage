<?php

/**
 * VideoPage category model
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Category extends Mage_Core_Model_Abstract
{

    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('videopage/category');
    }

    /**
     * @return array
     */
    public function getVideoIds()
    {
        return Mage::getResourceModel('videopage/category')->getVideoIds($this);
    }
}

?>
