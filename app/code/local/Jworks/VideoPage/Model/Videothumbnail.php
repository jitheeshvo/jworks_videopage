<?php

/**
 * VideoPage videothumbnail model
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Videothumbnail extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    }

    protected function _getValue(Varien_Object $row)
    {
        if ($row->getVideoThumbnail()) {
            $path = str_replace('/index.php/', '/', $this->getBaseUrl()) . 'media/videos/' . $row->getVideoThumbnail();
            return '<img src="' . $path . '" alt="' . $row->getVideoThumbnail() . '">';
        }
        return;
    }

}
