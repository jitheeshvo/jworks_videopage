<?php

/**
 * VideoPage video list resource model
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Resource_List extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        // Note that the video_id refers to the key field in your database table.
        $this->_init('videopage/videos', 'video_id');
    }
}

?>
