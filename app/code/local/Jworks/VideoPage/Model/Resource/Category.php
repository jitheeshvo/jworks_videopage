<?php

/**
 * VideoPage category resource model
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Resource_Category extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     *
     */
    public function _construct()
    {
        // Note that the category_id refers to the key field in your database table.
        $this->_init('videopage/category', 'category_id');
    }

    /**
     * @param Jworks_VideoPage_Model_Category $object
     * @return array
     */
    public function getVideoIds(Jworks_VideoPage_Model_Category $object)
    {
        $videoArray = array();
        if ($object->getCategoryId()) {
            $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('videopage/videogroup'))
                ->where('category_id = ?', $object->getCategoryId());

            if ($data = $this->_getReadAdapter()->fetchAll($select)) {
                foreach ($data as $row) {
                    $videoArray[$row['video_id']] = array('position' => $row['position']);
                }
            }
        }
        return $videoArray;
    }

    /**
     * Save video to the category group
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $category_id = $object->getCategoryId();
        $videos = $object->getVideos();
        if ($category_id && !empty($videos)) {
            $condition = $this->_getWriteAdapter()->quoteInto('category_id = ?', $category_id);
            $this->_getWriteAdapter()->delete($this->getTable('videopage/videogroup'), $condition);
            if (is_array($videos)) {
                foreach ($videos as $video_id => $video_data) {
                    $videoArray = array();
                    $videoArray['category_id'] = $category_id;
                    $videoArray['video_id'] = $video_id;
                    $videoArray['position'] = $video_data['position'];
                    $this->_getWriteAdapter()->insert($this->getTable('videopage/videogroup'), $videoArray);
                }
            }
        }
        parent::_beforeSave($object);
    }

    /**
     * @param $catid
     * @return array
     */
    public function getCategoryVideos($catid)
    {
        $videoArray = array();
        if ($catid) {
            $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('videopage/videogroup'), array('video_id'))
                ->where('category_id = ?', $catid)
                ->order('position');
            if ($data = $this->_getReadAdapter()->fetchAll($select)) {
                foreach ($data as $row) {
                    $videoArray[] = $row['video_id'];
                }
            }
        }
        return $videoArray;
    }

}

?>
