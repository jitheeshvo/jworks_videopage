<?php

/**
 * VideoPage adminhtml status model
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Status extends Varien_Object
{
    /**
     *
     */
    const STATUS_ENABLED = 1;
    /**
     *
     */
    const STATUS_DISABLED = 2;

    /**
     * @return array
     */
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('videopage')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('videopage')->__('Disabled')
        );
    }

}