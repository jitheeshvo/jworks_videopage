<?php

/**
 * VideoPage video list block
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Model_Session extends Mage_Core_Model_Session_Abstract
{
    /**
     * Jworks_VideoPage_Model_Session constructor.
     */
    public function __construct()
    {
        $this->init('videopage');
    }

}
