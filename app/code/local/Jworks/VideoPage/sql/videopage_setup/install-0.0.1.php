<?php
/**
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */

$installer = $this;
/* @var $installer Jworks_VideoPage_Model_Resource */

$installer->startSetup();

/**
 * Create table 'video_list'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('videopage/videos'))
    ->addColumn('video_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Video ID')
    ->addColumn('video_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array('nullable' => false))
    ->addColumn('video_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => false))
    ->addColumn('video_thumbnail', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array('nullable' => false))
    ->addColumn('video_embed_code', Varien_Db_Ddl_Table::TYPE_TEXT, null, array('nullable' => false))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'default' => '1',
    ))
    ->setComment('Video List Table');
$installer->getConnection()->createTable($table);


/**
 * Create table 'video_category'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('videopage/category'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Category ID')
    ->addColumn('video_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Entity ID')
    ->addColumn('category_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array('nullable' => false))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'default' => '1',
    ))
    ->setComment('Video Category Table');
$installer->getConnection()->createTable($table);

