<?php
/**
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */

$installer = $this;
/* @var $installer Jworks_VideoPage_Model_Resource */

$installer->startSetup();

/**
 * Add foreign key constraints to table 'video_group'
 */
$installer->getConnection()
    ->addForeignKey(
        $installer->getFkName('video_group', 'video_id', 'video_list', 'video_id'),
        $installer->getTable('videopage/videogroup'), 'video_id', $installer->getTable('videopage/videos'), 'video_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->getConnection()
    ->addForeignKey(
        $installer->getFkName('video_group', 'category_id', 'video_category', 'category_id'),
        $installer->getTable('videopage/videogroup'), 'category_id', $installer->getTable('videopage/category'), 'category_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);


