<?php
/**
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */

$installer = $this;
/* @var $installer Jworks_VideoPage_Model_Resource */

$installer->startSetup();

/**
 * Create table 'video_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('videopage/videogroup'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Category ID')
    ->addColumn('video_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Position')
    ->setComment('Video Category Group');
$installer->getConnection()->createTable($table);

