<?php

/**
 * VideoPage data helper extended
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Helper_Url extends Mage_Core_Helper_Url
{

    /**
     * Retrieve category video url
     *
     * @return string
     */
    public function getCategoryVideoUrl($cat_id)
    {
        return $this->_getUrl('videos/list/', array('cid' => $cat_id));
    }

    /**
     * Retrieve video url
     *
     * @return string
     */
    public function getVideoUrl($id)
    {
        return $this->_getUrl('videos/list/popup/', array('id' => $id));
    }

}

?>
