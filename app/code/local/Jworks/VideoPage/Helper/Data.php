<?php

/**
 * VideoPage data helper extended
 *
 * @category   Jworks
 * @package    Jworks_VideoPage
 */
class Jworks_VideoPage_Helper_Data extends Mage_Core_Helper_Abstract
{
    /*
     * Recursively searches and replaces all occurrences of search in subject values replaced with the given replace value
     * @param string $search The value being searched for
     * @param string $replace The replacement value
     * @param array $subject Subject for being searched and replaced on
     * @return array Array with processed values
     */

    public function recursiveReplace($search, $replace, $subject)
    {
        if (!is_array($subject))
            return $subject;

        foreach ($subject as $key => $value)
            if (is_string($value))
                $subject[$key] = str_replace($search, $replace, $value);
            elseif (is_array($value))
                $subject[$key] = self::recursiveReplace($search, $replace, $value);

        return $subject;
    }

    /**
     * Function to get the configuration for videopage
     *
     * @staticvar array $config
     * @param string $configName
     * @return Mixed array of configuration/ string of specified configuration
     */
    public function getConfig($configName = '', $root = 'videopagesections')
    {
        static $config = null;
        !isset($config) && $config = Mage::getStoreConfig($root);
        if (empty($configName)) {
            return $config;
        } else {
            return isset($config[$configName]) ? $config[$configName] : '';
        }
    }

    /**
     * Function to resize image
     *
     * @param string $filename
     * @param int $width
     * @param int $height
     * @return string resized image
     */
    function resize($filename, $width, $height)
    {
        if (empty($filename)) {
            return '';
        }

        $path = explode('/', $filename);
        $filename = $path[1];
        $path_old = $path[0];

        $newFolder = $width . 'x' . $height;
        $basePath = Mage::getBaseDir('media');
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, array('_secure' => true));
        $imageOldPath = $basePath . DS . $path_old . DS . $filename;
        $imageOldPath = str_replace('\\', '/', $imageOldPath);
        $imageResizedPath = $basePath . DS . $path_old . DS . $newFolder . DS;
        if (!is_dir($imageResizedPath))
            mkdir($imageResizedPath);
        $imageResizedUrl = $baseUrl . $path_old . '/' . $newFolder . '/';
        //  $imageResizedUrl = str_replace('\\', '/', $imageResizedUrl);
        if (file_exists($imageResizedPath . $filename)) {
            return $imageResizedUrl . $filename;
        }
        if (file_exists($imageOldPath)) {
            $imageObj = new Varien_Image($imageOldPath);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            $imageObj->keepTransparency(TRUE);
            $imageObj->quality(200);
            $imageObj->resize($width, $height);
            $imageObj->save($imageResizedPath, $filename);
            return $imageResizedUrl . $filename;
        }
    }
}

?>
