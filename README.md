Jworks VideoPage
================

    Author Jitheesh V O <jitheeshvo@gmail.com>
    
    copyright Copyright (c) 2017 Jworks

**Overview**
------
Magento Jworks_VideoPage is the easiest and most effective way to list and show a collection of your videos.


**Features Full List:**

- Eye-catching video thumbnail images to attract customers
- fancybox support to show videos in pop-up
- Group videos by category

**_For Customers_**

- Can see video listing page on a website with pagination
- Can filter videos by categories
- They can play video in fancybox pup-up

**_Others_**

- 100% Open source
- Easy to install and configure
- User-friendly interface

**_For Admins_**

- Create videos with many information such as description, video image, Youtube ID / Embed Code etc
- Can enable/disable videos individually 
- Create video categories
- Can assign videos into video list categories.




Installation
------------
- unzip jworks_videopage_m1.zip file and copy app/ skin/ directories into project root folder
- Clear cache
- Run code compilation if it is enabled


Release Notes 
---
0.0.3:
    
   - Fixes: forginkey constrain 

0.0.2:
    
   - Category support added
   - Feature to include video positions   

0.0.1:
    
   - Module created